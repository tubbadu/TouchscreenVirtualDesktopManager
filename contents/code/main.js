/*
*  Copyright 2024 Tubbadu <tubbadu@gmail.com>
*
*  General Public License for more details.
*
*  You should have received a copy of the GNU General Public License
*  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

workspace.windowActivated.connect(function(win) {
    if(win && win.noBorder){
        win.setMaximize(false, false);
        win.setMaximize(true, true); // this should make sure that a maximized window stays maximized
    }
});

workspace.windowAdded.connect(function(win) {
    if(isNormalWindow(win)){
        windowCreated(win);
    }
});

workspace.windowRemoved.connect(function(win) {
    console.log("win", win, "removed!")
    purgeEmptyDesktops(workspace.desktops)
});



function windowCreated(win){
    // we already checked if isNormal
    console.log("window ", win.resourceClass, " created!")
    let currentDesktopIndex = getDesktopIndex(workspace.currentDesktop);
    workspace.createDesktop(currentDesktopIndex+1, "Desktop - " + win.resourceClass);
    win.desktops = [workspace.desktops[currentDesktopIndex+1]];
    
    win.noBorder = true;
    win.setMaximize(true, true);
    workspace.activeWindow = win;
}

function purgeEmptyDesktops(desktops){
    console.log("purging empty desktops")
    for(let i = 1; i < desktops.length; i++){ // skip first Desktop
        const desktop = desktops[i];
        if(isEmpty(desktop)){
            // remove it
            console.log("removing empty desktop:", i, desktop)
            workspace.removeDesktop(desktop);
            // it should (should!) automatically move to the previous one if the removed desktop was the current one
        }
    }
}

function isEmpty(desktop){
    for(const win of workspace.windowList()){
        if(arraysEqual(win.desktops, [desktop])){ // if the window belongs to this and only this desktop
            console.log("desktop isEmpty: false")
            return false;
        }
    }
    console.log("desktop isEmpty: true")
    return true;
}

function isNormalWindow(win){
    if(!win.specialWindow && !win.dialog && win.maximizable && win.normalWindow){
        return true;
    } else {
        return false;
    }
}

function getDesktopIndex(desktop){
    return workspace.desktops.indexOf(desktop);;
}

function arraysEqual(a, b) {
    if (a === b) return true;
    if (a == null || b == null) return false;
    if (a.length !== b.length) return false;
    
    // If you don't care about the order of the elements inside
    // the array, you should sort both arrays here.
    // Please note that calling sort on an array will modify that array.
    // you might want to clone your array first.
    
    for (var i = 0; i < a.length; ++i) {
        if (a[i] !== b[i]) return false;
    }
    return true;
}