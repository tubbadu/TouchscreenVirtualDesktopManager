# TouchscreenVirtualDesktopManager

A kwin script to automatically manage windows when using a touchscreen

> NOTE! This is the Plasma 6 version! you can find the legacy plasma 5 version in the `plasma5-branch`

## install

```bash
cd ~/.local/share/kwin/scripts # navigate to the kwin scripts folder
git clone https://codeberg.org/tubbadu/TouchscreenVirtualDesktopManager # clone this repo
```

## setup

- enable this script in settings > Window Management > KWin Scripts
- in settings > Workspace behavior > Desktop effect, enable "Overview" and "Present Windows" effect, and set the animation for virtual desktop change to "Slide"

## how does it work

Whenever a new window is created, this script will automatically create a new virtual desktop, and then move the new window there, maximized and borderless.
Whenever a virtual desktop except the first one is left empty, with no windows, it is automatically removed.

This will make sure that every window has its own virtual desktop.

The script should ignore popups and un-maximizable windows.

You can then switch between desktops (and so, between applications) with a 3 finger right or left slide gesture, or activate the overview or present window with a 3 finger up or down gesture.

## known issues and todos

- [ ] write a better readme
- [x] sometimes there is no empty "desktop 0"
